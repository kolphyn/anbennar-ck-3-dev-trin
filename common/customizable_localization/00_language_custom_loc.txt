﻿# Anbennar: commenting vanilla stuff
# Anbennar TODO: add our stuff to these custom loc. Dont add new ones in here, put them in another file if needed

# IberiaSacredLanguage = { # Anbennar
	# type = character
	
	# text = {
		# trigger = { faith.religion = religion:christianity_religion }
		# localization_key = language_latin_name #Latin
	# }
	
	# text = {
		# trigger = { faith.religion = religion:judaism_religion }
		# localization_key = IberiaSacredLanguage_hebrew #Hebrew
	# }
	
	# text = {
		# trigger = { faith.religion = religion:islam_religion }
		# localization_key = language_arabic_name #Arabic
	# }
# }

# IberiaRomanceLanguage = { # Anbennar
	# type = character
	
	# text = {
		# trigger = {	faith.religion = religion:christianity_religion }
		# localization_key = IberiaRomanceLanguage_castilian #Castilian
	# }
	
	# #text = { # I couldn't find evidence of a unique Jewish dialect until after the expulsion of the jews in 1492, when Ladino was coined - AN
	# #	trigger = { religion = religion:judaism_religion }
	# #	localization_key = IberiaRomanceLanguage_sephardi #Sephardi
	# #}
	
	# text = {
		# trigger = { faith.religion = religion:islam_religion }
		# localization_key = IberiaRomanceLanguage_mozarabic #Mozarabic #TO_DO_CD: This should be an actual language for our Mozarabs, but would require a culture creation - AN
	# }
# }

#Key to pick out liturgical language of the faith of a province or character - if one exists
#Uses "Archaic-[spoken language]" as fallback
#Specific Fallbacks exists for:
#Catholic
#Orthodox
#Nestorian
#Judaism
#Islam
#Buddhism
#Hinduism
#Tibetan
#Taoism
#Jainism
#Zoroastrianism
#Greco-Roman

GetFaithSacredLanguage = {
	type = all
	
	# text = { # Anbennar
		# trigger = {
			# OR = {
				# faith = faith:catholic
				# faith = faith:conversos
				# faith = faith:lollard
				# faith = faith:insular_celtic
				# faith = faith:mozarabic_church
			# }
		# }
		# localization_key = language_latin_name #Latin
	# }

	# text = { # Anbennar
		# trigger = {
			# faith = faith:cathar
		# }
		# localization_key = language_occitano_romance_name #Occitan
	# }

	# text = { # Anbennar
		# trigger = { faith = faith:nestorian }
		# localization_key = language_aramaic_name #Armaic
	# }

	# text = { # Anbennar
		# trigger = { faith = faith:armenian_apostolic }
		# localization_key = language_armenian_name #Armenian
	# }

	# text = { # Anbennar
		# trigger = {
			# OR = {
				# faith = faith:orthodox
				# faith = faith:coptic
				# religion = religion:christianity_religion #fallback for all Christian Faiths
			# }
		# }
		# localization_key = language_greek_name #Greek
	# }
	
	# text = { # Anbennar
		# trigger = { religion = religion:judaism_religion }
		# localization_key = language_israelite_name #Hebrew
	# }
	
	# text = { # Anbennar
		# trigger = { religion = religion:islam_religion }
		# localization_key = language_arabic_name #Arabic
	# }
	
	# text = { # Anbennar
		# trigger = { religion = religion:zoroastrianism_religion }
		# localization_key = language_avestan_name #Avestan
	# }

	# text = { # Anbennar
		# trigger = { faith = faith:theravada }
		# localization_key = language_pali_name #Pali
	# }

	# text = { # Anbennar
		# trigger = {
			# OR = {
				# faith = faith:lamaism
				# religion = religion:bon_religion
				# AND = {
					# faith = faith:vajrayana
					# culture = {
						# has_cultural_pillar = heritage_tibetan
					# }
				# }
			# }
		# }
		# localization_key = language_classical_tibetan_name #Classical Tibetan
	# }

	# text = { # Anbennar
		# trigger = {
			# religion = religion:taoism_religion
		# }
		# localization_key = language_chinese_name
	# }

	# text = { #Could also be Vedic & Old Tamil # Anbennar
		# trigger = {
			# OR = {
				# religion = religion:hinduism_religion
				# religion = religion:buddhism_religion
				# religion = religion:jainism_religion
			# }
		# }
		# localization_key = language_sanskrit_name #Sanskrit
	# }

	# text = { # Anbennar
		# trigger = {
			# religion = religion:hellenism_religion
		# }
		# localization_key = language_greek_name #Greek
	# }

	text = {
		trigger = {
			always = yes
		}
		fallback = yes
		localization_key = fallback_liturgical_province_language #Archaic-X - based on culture instead of Faith.
	}
}


GetFaithSacredLanguageCharacter = {
	type = character
	
	# text = { # Anbennar
		# trigger = {
			# OR = {
				# faith = faith:catholic
				# faith = faith:conversos
				# faith = faith:lollard
				# faith = faith:insular_celtic
				# faith = faith:mozarabic_church
			# }
		# }
		# localization_key = language_latin_name #Latin
	# }

	# text = { # Anbennar
		# trigger = {
			# faith = faith:cathar
		# }
		# localization_key = language_occitano_romance_name #Occitan
	# }

	# text = { # Anbennar
		# trigger = { faith = faith:nestorian }
		# localization_key = language_aramaic_name #Armaic
	# }

	# text = { # Anbennar
		# trigger = { faith = faith:armenian_apostolic }
		# localization_key = language_armenian_name #Armenian
	# }

	# text = { # Anbennar
		# trigger = {
			# OR = {
				# faith = faith:orthodox
				# faith = faith:coptic
				# religion = religion:christianity_religion #fallback for all Christian Faiths
			# }
		# }
		# localization_key = language_greek_name #Greek
	# }
	
	# text = {  # Anbennar
		# trigger = { religion = religion:judaism_religion }
		# localization_key = language_israelite_name #Hebrew
	# }
	
	# text = { # Anbennar
		# trigger = { religion = religion:islam_religion }
		# localization_key = language_arabic_name #Arabic
	# }
	
	# text = { # Anbennar
		# trigger = { religion = religion:zoroastrianism_religion }
		# localization_key = language_avestan_name #Avestan
	# }

	# text = { # Anbennar
		# trigger = { faith = faith:theravada }
		# localization_key = language_pali_name #Pali
	# }

	# text = { # Anbennar
		# trigger = {
			# OR = {
				# faith = faith:lamaism
				# religion = religion:bon_religion
				# AND = {
					# faith = faith:vajrayana
					# culture = {
						# has_cultural_pillar = heritage_tibetan
					# }
				# }
			# }
		# }
		# localization_key = language_classical_tibetan_name #Classical Tibetan
	# }

	# text = { # Anbennar
		# trigger = {
			# religion = religion:taoism_religion
		# }
		# localization_key = language_chinese_name
	# }

	# text = { #Could also be Vedic & Old Tamil # Anbennar
		# trigger = {
			# OR = {
				# religion = religion:hinduism_religion
				# religion = religion:buddhism_religion
				# religion = religion:jainism_religion
			# }
		# }
		# localization_key = language_sanskrit_name #Sanskrit
	# }

	# text = { # Anbennar
		# trigger = {
			# religion = religion:hellenism_religion
		# }
		# localization_key = language_greek_name #Greek
	# }
	text = {
		trigger = {
			always = yes
		}
		fallback = yes
		localization_key = fallback_liturgical_character_language #Archaic-X - based on culture instead of Faith.
	}
}
