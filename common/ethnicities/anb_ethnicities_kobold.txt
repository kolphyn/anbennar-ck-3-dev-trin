﻿#Credit to the Elder Kings 2 team for using their Argonian as a base

kobold_redscale = {	
	template = "ethnicity_template"
	using = "redscale"
	visible = yes

	skin_color = {
		90 = { 0.7 0.3 1.0 0.5 }
	}

	skin_color_hue = {
		#1 = { name = vanilla_skin_hue		range = { 0.0 1.0 } }

		#1 = { name = skin_color_hue_yellow		range = { 0.0 1.0 } }

		#2 = { name = skin_color_hue_green		range = { 0.0 1.0 } }

		#1 = { name = skin_color_hue_blue		range = { 0.0 1.0 } }

		#1 = { name = skin_color_hue_purple		range = { 0.0 1.0 } }

		#1 = { name = skin_color_hue_pink		range = { 0.0 1.0 } }

		1 = { name = skin_color_hue_red			range = { 0.6 1.0 } }

		#1 = { name = skin_color_hue_orange		range = { 0.0 1.0 } }

		#1 = { name = skin_color_hue_black		range = { 0.0 1.0 } }
	}

	skin_color_saturation = {
		1 = { name = skin_color_saturation		range = { 0.75 1.0 } }
	}
	
	gene_height = {

		2 = { name = kobold_height		range = { 0.25 0.27 }		} #kobold short 	(-0.2)
		12 = { name = kobold_height		range = { 0.27 0.31 }		} #kobold		
		4 = { name = kobold_height		range = { 0.31 0.33 }		} #kobold tall		(+0.2)
		1 = { name = kobold_height		range = { 0.33 0.35 }		} #kobold very tall (+0.2)

	}

	leg_shape = {
		1 = { name = kobold_digitigrade_legs range = { 0.0 0.1 } }
	}
	
	tails = {
		1 = { name = kobold_tail range = { 0.0 0.1 } }
	}

    tail_length = {
		1 = { name = tail_length range = { 0.7 1.0 } }
    }

	eye_color = {
		# Yellow colours
		10 = { 0.35 0.15 0.45 0.3 }
		#black
		#1 = { 1.0 1.0 1.0 1.0 }
	}

	eye_color_saturation = {
		1 = { name = eye_color_saturation		range = { 0.40 0.70 } }
	}

	# eye_sclera = {
	# 	10 = {	name = human_sclera			range = { 0.0 0.0 }	}
	# }

	eye_pupil = {
		10 = {	name = kobold_pupils			range = { 0.10 0.15 }	}
	}

	hair_color = {
		#Full shade spectrum
		10 = { 0.2 0.9 0.2 1.0 }
	}

	hair_color_hue = {
		1 = { name = hair_color_100_saturation		range = { 0.0 1.0 } }
	}

	monstrous_race_features = {
		5 = {  name = kobold_features			range = { 1.0 1.0 }	}
	}

	gene_chin_forward = {
        5 = { name = chin_forward_pos    range = { 0.35 0.45 }    }
    }

	gene_chin_height = {
        5 = { name = chin_height_pos    range = {0.1 0.2  }    }
    }

    gene_chin_width = {
        5 = { name = chin_width_pos    range = {0.3 0.5  }    }
    }

    gene_eye_angle = {
        5 = { name = eye_angle_pos    range = {0.5 0.5  }    }
    }

	gene_eye_depth = {
        5 = { name = eye_depth_pos    range = {0.0 0.5  }    }
    }

	gene_eye_height = {
        5 = { name = eye_height_pos    range = {0.25 0.8  }    }
    }

	gene_eye_distance = {
        5 = { name = eye_distance_pos    range = {0.3 0.5  }    }
    }

	gene_forehead_angle = {
        5 = { name = forehead_angle_pos    range = {0.3 0.8  }    }
    }

    gene_forehead_brow_height = {
        5 = { name = forehead_brow_height_pos    range = {0.2 0.7  }    }
    }

    gene_forehead_roundness = {
        5 = { name = forehead_roundness_pos    range = {0.3 0.6  }    }
    }

	gene_forehead_width = {
        5 = { name = forehead_width_pos    range = {0.0 0.5  }    }
    }

	gene_forehead_height = {
        5 = { name = forehead_height_pos    range = {0.5 1.0  }    }
    }

	gene_head_height = {
        5 = { name = head_height_pos    range = {0.0 0.35  }    }
    }

    gene_head_width = {
        5 = { name = head_width_pos    range = {0.0 0.3  }    }
    }

    gene_head_profile = {
        5 = { name = head_profile_pos    range = {0.5 0.7  }    }
    }

    gene_head_top_height = {
        5 = { name = head_top_height_pos    range = {0.3 0.7  }    }
    }

    gene_head_top_width = {
        5 = { name = head_top_width_pos    range = {0.6 1.0  }    }
    }

	gene_jaw_angle = {
        5 = { name = jaw_angle_pos    range = {0.45 0.5  }    }
    }

    gene_jaw_forward = {
        5 = { name = jaw_forward_pos    range = {0.5 0.6  }    }
    }

	gene_jaw_height = {
        5 = { name = jaw_height_pos    range = {0.8 1.0  }    }
    }

    gene_jaw_width = {
        5 = { name = jaw_width_pos    range = {0.25 1.0  }    }
    }

	gene_mouth_corner_depth = {
        5 = { name = mouth_corner_depth_pos    range = {0.35 1.0  }    }
    }

    gene_mouth_corner_height = {
        5 = { name = mouth_corner_height_pos    range = {0.5 0.5  }    }
    }

    gene_mouth_forward = {
        5 = { name = mouth_forward_pos    range = {0.5 0.6  }    }
    }

    gene_mouth_height = {
        5 = { name = mouth_height_pos    range = {0.3 0.5  }    }
    }

    gene_mouth_width = {
        5 = { name = mouth_width_pos    range = {0.5 0.7  }    }
    }

    gene_mouth_upper_lip_size = {
        5 = { name = mouth_upper_lip_size_pos    range = {0.4 0.5  }    }
    }

	gene_mouth_lower_lip_size = {
        5 = { name = mouth_lower_lip_size_pos    range = {0.0 0.0  }    }
    }

	gene_neck_length = {
        5 = { name = neck_length_pos    range = {0.5 0.8  }    }
    }

    gene_neck_width = {
        5 = { name = neck_width_tiny    range = {0.0 0.1  }    }
    }

    gene_bs_cheek_forward = {
        5 = { name = cheek_forward_pos    range = { 0.0  1.0  }    }
		5 = { name = cheek_forward_neg    range = { 0.0  1.0  }    }
    }

    gene_bs_cheek_height = {
        5 = { name = cheek_height_pos    range = { 0.0  1.0  }    }
    }

	gene_bs_cheek_width = {
        5 = { name = cheek_width_pos    range = { 0.0  1.0  }    }
    }

    gene_bs_ear_size = {
        5 = { name = ear_size_pos    range = { 0.0  0.5  }    }
    }

    gene_bs_ear_bend = {
        5 = { name = ear_both_bend_pos    range = { 0.0  0.15  }    }
    }

    gene_bs_ear_outward = {
        5 = { name = ear_outward_pos    range = { 0.0  0.0  }    }
    }

	gene_bs_eye_size = {
        5 = { name = eye_size_pos    range = { 0.0  1.0  }    }
    }

    gene_bs_eye_upper_lid_size = {
        5 = { name = eye_upper_lid_size_pos    range = { 0.0  1.0  }    }
    }

    gene_bs_forehead_brow_curve = {
        1 = { name = forehead_brow_curve_pos    range = { 0.0  1.0  }    }
    }

    gene_bs_forehead_brow_forward = {
        1 = { name = forehead_brow_forward_pos    range = { 0.0  1.0  }    }
    }

    gene_bs_forehead_brow_inner_height = {
        5 = { name = forehead_brow_inner_height_pos    range = { 0.0  1.0  }    }
    }

    gene_bs_forehead_brow_outer_height = {
        5 = { name = forehead_brow_outer_height_pos    range = { 0.0  1.0  }    }
    }

	gene_bs_forehead_brow_width = {
        5 = { name = forehead_brow_width_pos    range = { 0.0  1.0  }    }
    }

	gene_bs_jaw_def = {
        5 = { name = jaw_def_pos    range = { 0.0 1.0  }    }
		5 = { name = jaw_def_neg    range = { 0.0 1.0  }    }
    }

	gene_bs_mouth_philtrum_shape = {
        5 = { name = mouth_philtrum_shape_pos    range = { 0.0  1.0  }    }
		5 = { name = mouth_philtrum_shape_neg    range = { 0.0  1.0  }    }
    }

	gene_bs_mouth_philtrum_width = {
        5 = { name = mouth_philtrum_width_pos    range = { 0.0  1.0  }    }
		5 = { name = mouth_philtrum_width_neg   range = { 0.0  1.0  }    }
    }

    gene_bs_mouth_upper_lip_def = {
        5 = { name = mouth_upper_lip_def_pos    range = { 0.0  1.0  }    }
        
    }

    gene_bs_mouth_upper_lip_full = {
        5 = { name = mouth_upper_lip_full_pos    range = { 0.0  1.0  }    }
        5 = { name = mouth_upper_lip_full_neg    range = { 0.0  1.0  }    }
    }

    gene_bs_mouth_upper_lip_profile = {
        5 = { name = mouth_upper_lip_profile_neg    range = { 0.0  1.0  }    }
        5 = { name = mouth_upper_lip_profile_pos    range = { 0.0  1.0  }    }
    }

    gene_bs_mouth_upper_lip_width = {
        5 = { name = mouth_upper_lip_width_neg    range = { 0.0  0.0  }    }
    }

    gene_bs_mouth_upper_lip_width = {
        5 = { name = mouth_upper_lip_width_pos    range = { 0.0  0.1  }    }
    }

    gene_bs_nose_forward = {
        5 = { name = nose_forward_pos    range = { 0.0  0.5  }    }
    }

    gene_bs_nose_height = {
        5 = { name = nose_height_neg    range = { 0.0  0.45  }    }
        5 = { name = nose_height_pos    range = { 0.0  1.0  }    }
    }

    gene_bs_nose_nostril_height = {
        5 = { name = nose_nostril_height_neg    range = { 0.0  1.0  }    }
        5 = { name = nose_nostril_height_pos    range = { 0.0  1.0  }    }
    }

    gene_bs_nose_nostril_width = {
        5 = { name = nose_nostril_width_pos    range = { 0.0  0.5  }    }
    }

    gene_bs_nose_profile = {
        5 = { name = nose_profile_neg    range = { 0.0  0.35  }    }
        5 = { name = nose_profile_pos    range = { 0.0  0.3  }    }
    }

    gene_bs_nose_ridge_angle = {
        5 = { name = nose_ridge_angle_neg    range = { 0.0  0.5  }    }
        5 = { name = nose_ridge_angle_pos    range = { 0.0  1.0  }    }
    }

    gene_bs_nose_ridge_width = {
        5 = { name = nose_ridge_width_pos    range = { 0.0  1.0  }    }
        5 = { name = nose_ridge_width_pos    range = { 0.0  1.0  }    }
    }

    gene_bs_nose_size = {
        5 = { name = nose_size_pos    range = { 0.0  1.0  }    }
    }

    gene_bs_nose_tip_angle = {
        5 = { name = nose_tip_angle_pos    range = { 0.75  1.0  }    }
    }

    gene_bs_nose_tip_width = {
        5 = { name = nose_tip_width_pos    range = { 0.5  1.0  }    }
    }

    face_detail_cheek_def = {
        5 = {  name = cheek_def_01             range = { 0.0  1.0  }     }
        5 = {  name = cheek_def_02             range = { 0.0  1.0  }     }
    }

    face_detail_nose_ridge_def = {
        5 = {  name = nose_ridge_def_neg             range = { 0.0  1.0  }     }
    }

    face_detail_temple_def = {
        5 = {  name = temple_def             range = { 0.0  1.0  }     }
    }

    
    expression_other = {
        5 = {  name = cheek_wrinkles_left_01             range = { 0.0  0.0  }     }
    }


	gene_bs_body_type = {

		# 0 = { name = body_average 	range = { 0.0 0.0 }		 }
        5 = { name = body_fat_head_fat_low   range = { 0.4 0.45 }      }
        25 = { name = body_fat_head_fat_low   range = { 0.45 0.55 }   traits = { beauty_1 beauty_2 beauty_3 }   }
        5 = { name = body_fat_head_fat_low   range = { 0.55 0.6 }      }

        5 = { name = body_fat_head_fat_medium   range = { 0.4 0.45 }      }
        25 = { name = body_fat_head_fat_medium   range = { 0.45 0.55 }      }
        5 = { name = body_fat_head_fat_medium   range = { 0.55 0.6 }      }

        5 = { name = body_fat_head_fat_full   range = { 0.4 0.45 }      }
        25 = { name = body_fat_head_fat_full   range = {0.45 0.55 }      }
        5 = { name = body_fat_head_fat_full   range = { 0.55 0.6 }      }

	}

    gene_bs_body_shape = {
        10 = { name = body_shape_average     range = { 0.0 0.0 }      }
        10 = { name = body_shape_hourglass_half     range = { 0.0 0.0 }      }
        10 = { name = body_shape_hourglass_full     range = { 0.0 0.0 }      }
        15 = { name = body_shape_rectangle_half     range = { 0.0 0.0 }      }
        5 = { name = body_shape_rectangle_full     range = { 0.0 0.0 }      }
        15 = { name = body_shape_triangle_half     range = { 0.0 0.0 }      }
        5 = { name = body_shape_triangle_full     range = { 0.0 0.0 }      }

        10 = { name = body_shape_average     range = { 0.0 0.25 }      }
        10 = { name = body_shape_hourglass_half     range = { 0.0 0.4 }      }
        10 = { name = body_shape_hourglass_full     range = { 0.0 0.4 }      }
        15 = { name = body_shape_rectangle_half     range = { 0.0 0.4 }      }
        5 = { name = body_shape_rectangle_full     range = { 0.1 0.5 }      }
        15 = { name = body_shape_triangle_half     range = { 0.0 0.4 }      }
        5 = { name = body_shape_triangle_full     range = { 0.1 0.5 }      }
    }

    gene_bs_bust = {
        5 = { name = bust_default    range = { 0.0 0.0 }    }
    }	 

    gene_age = {
        10 = { name = old_beauty_1      range = { 0.0 1.0 } }
    }

	gene_eyebrows_shape = {
		5 = {  name = no_eyebrows			range = { 0.5 1.0 }	}
	}

	gene_eyebrows_fullness = {
		5 = {  name = no_eyebrows			range = { 0.25 0.5 }	}
	}

	gene_body_hair = {
		5 = {  name = body_hair_sparse			range = { 0.0 0.0 }	}
	}

	monstrous_head_attachment_01_variation = {
		5 = {  name = attachment_01_variation			range = { 0.4 1.0 }	}
	}

	monstrous_head_attachment_01 = {
		5 = {  name = kobold_horns			range = { 0.0 1.0 }	}
	}

    monstrous_head_attachment_02_variation = {
		5 = {  name = attachment_02_variation			range = { 0.0 1.0 }	}
	}

	monstrous_head_attachment_02 = {
		5 = {  name = kobold_jaw_spikes			range = { 0.0 1.0 }	}
	}

    monstrous_head_attachment_03_variation = {
		5 = {  name = attachment_03_variation			range = { 0.0 1.0 }	}
	}

	monstrous_head_attachment_03 = {
		5 = {  name = kobold_cheek_spikes			range = { 0.0 1.0 }	}
	}

    monstrous_head_attachment_04_variation = {
		5 = {  name = attachment_04_variation			range = { 0.0 1.0 }	}
	}

	# monstrous_head_attachment_04 = {
	# 	5 = {  name = kobold_eyebrow_spikes			range = { 0.0 1.0 }	}
	# }

	eyelashes_accessory = {
		5 = { name = no_eyelashes 			range = { 0.0 0.0 } }
	}

	gene_age = {
		5 = { name =  old_beauty_1	range = { 0.0 0.0 } }
	}

	complexion = {
		10 = {	name = complexion_no_face			range = { 1.0 1.0 }		}
	}

	monstrous_race_decal_layer_01 = {
		10 = {	name = kobold_shading_1		range = { 0.6 1.0 }		}
	}

	monstrous_race_decal_layer_02 = {
		10 = {	name = kobold_scales_patterns_01			range = { 0.15 0.3 }		}
	}

    # gene_forehead_inner_brow_width = {
    #     5 = {  name = inner_brow_width             range = { 0.0 0.8 }     }
    # }

    # gene_bs_mouth_lower_lip_profile = {
    #     5 = {  name = lower_lip_profile             range = { 0.0 0.0 }     }
    # }
    # teeth_accessory = {
    #     1 = { name = no_teeth                   range = { 0.0 0.0 } }
    # }

}

kobold_darkscale = {
    template = "kobold_redscale"

	skin_color = {
		90 = { 0.7 0.6 1.0 0.75 }
	}
}

kobold_bluescale = {
    template = "kobold_redscale"

	skin_color = {
		90 = { 0.4 0.3 0.8 0.6 }
	}

	skin_color_hue = {
		#1 = { name = vanilla_skin_hue		range = { 0.0 1.0 } }

		#1 = { name = skin_color_hue_yellow		range = { 0.0 1.0 } }

		#2 = { name = skin_color_hue_green		range = { 0.0 1.0 } }

		1 = { name = skin_color_hue_blue		range = { 0.6 0.8 } }

		#1 = { name = skin_color_hue_purple		range = { 0.0 1.0 } }

		#1 = { name = skin_color_hue_pink		range = { 0.0 1.0 } }

		#1 = { name = skin_color_hue_red			range = { 0.5 1.0 } }

		#1 = { name = skin_color_hue_orange		range = { 0.0 1.0 } }

		#1 = { name = skin_color_hue_black		range = { 0.0 1.0 } }
	}
}

kobold_greenscale = {
    template = "kobold_redscale"

	skin_color = {
		90 = { 0.0 0.5 0.8 0.7 }
	}

	skin_color_hue = {
		#1 = { name = vanilla_skin_hue		range = { 0.0 1.0 } }

		#1 = { name = skin_color_hue_yellow		range = { 0.0 1.0 } }

		1 = { name = skin_color_hue_green		range = { 0.5 1.0 } }

		#1 = { name = skin_color_hue_blue		range = { 0.5 0.8 } }

		#1 = { name = skin_color_hue_purple		range = { 0.0 1.0 } }

		#1 = { name = skin_color_hue_pink		range = { 0.0 1.0 } }

		#1 = { name = skin_color_hue_red			range = { 0.5 1.0 } }

		#1 = { name = skin_color_hue_orange		range = { 0.0 1.0 } }

		#1 = { name = skin_color_hue_black		range = { 0.0 1.0 } }
	}
}