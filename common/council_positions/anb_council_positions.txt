﻿councillor_mage = {
		
	name = {
		first_valid = {
			#Bulwari culture
			triggered_desc = {
				trigger = {
					scope:councillor_liege = {
						culture = { has_cultural_pillar = heritage_bulwari }
					}
				}
				desc = councillor_magi
			}
			#Add more triggered_desc for other cultures and races

			desc = councillor_mage
		}
	}
	
	tooltip = game_concept_court_mage_desc

	valid_character = {
		exists = root.liege_or_court_owner
		can_be_marshal_trigger = { COURT_OWNER = root.liege_or_court_owner }
	}

	on_get_position = {
		got_council_position_effect = yes
	}

	on_fired_from_position = {
		fired_from_council_position_effect = yes
	}

	#Figure out a fitting custom portrait animation
	portrait_animation = chaplain
}
