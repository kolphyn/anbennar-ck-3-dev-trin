k_elkmarch = {
	1000.1.1 = {
		change_development_level = 8
	}
	1018.9.15 = {
		holder = hardoak_0001 #Sybille Locke
	}
}

d_elkmarch = {
	1000.1.1 = {
		change_development_level = 7
	}
}

c_elkwood = {
	1018.9.15 = {
		holder = hardoak_0001 #Sybille Locke
	}
}

c_hardoaks = {
	1000.1.1 = {
		change_development_level = 8
	}
	1018.9.15 = {
		holder = hardoak_0001 #Sybille Locke
	}
}

c_twofork = {
	970.1.1 = {
		holder = elkhorn_0001 # Fordo Elkhorn
	}
	1001.1.1 = {
		holder = elkhorn_0002 # PLACEHOLDER Elkhorn
	}
	1018.9.15 = {
		holder = hardoak_0001 #Sybille Locke
	}
}

c_the_approach = {
	1018.9.15 = {
		holder = thornfoot_0002 # Elias Thornfoot
		liege = k_elkmarch
	}
}

d_uelaire = {
	1005.1.1 = {
		holder = bymouth_0001 # Malmo Bymouth
	}
	1013.11.7 = {
		holder = 43 #Eustace sil Uelaire
	}
}

c_uelaire = {
	1000.1.1 = {
		change_development_level = 9
	}
	1005.1.1 = {
		holder = bymouth_0001 # Malmo Bymouth
	}
	1013.11.7 = {
		holder = 43 #Eustace sil Uelaire
	}
}

c_westmere = {
	1013.11.7 = {
		holder = 43 #Eustace sil Uelaire
	}
}

c_highwind = {
	1005.1.1 = {
		holder = bymouth_0001 # Malmo Bymouth
	}
	1013.11.7 = {
		holder = 43 #Eustace sil Uelaire
	}
}

c_newacre = {
	1000.1.1 = {
		holder = newleaf_0001 # Nook Newleaf, former Count of Newacre
	}
	1013.11.7 = {
		holder = 43 #Eustace sil Uelaire
	}
}