﻿##### MAINTHEME

# Track name is used to determine priority - last track after sorting alphabetically is chosen as main track
zmain_theme_track = {
	music = "file:/music/anb_music/Memories_of_Aelantir.ogg"
	can_be_interrupted = yes
}
